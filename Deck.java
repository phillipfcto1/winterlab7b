import java.util.Random;
public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		this.rng = new Random();
		this.numberOfCards = 52;
		this.cards = new Card[52];
		
		int counter = 0;
		for(Suit i : Suit.values()){
			for(Rank j : Rank.values()){
				this.cards[counter] = new Card(i, j);
			counter++;
			}
		}
		// for(int i = 0; i < suits.length;i++){
			// for(int j = 0; j < values.length;j++){
				// this.cards[a] = new Card(suits[i], values[j]);
				// a++;
			// }
		// }
	}
	public int length(){
		return this.numberOfCards;
	}
	public Card drawTopCard(){
		Card topCard = this.cards[this.numberOfCards-1];
		this.numberOfCards--;
		return topCard;
		
}	
	public String toString(){
		String result = "";
		for(int i = 0; i < numberOfCards; i++){
			result += this.cards[i] + "\n";
		}
		return result;

		}
	public void shuffle(){
		for(int i = 0; i < numberOfCards; i++){
			int pos = this.rng.nextInt(i + 1);
			Card temp = cards[i];
			this.cards[i] = this.cards[pos];
			this.cards[pos] = temp;
		}
	}

}
public enum Suit{
	Hearts(0.4),
	Spades(0.3),
	Diamonds(0.2),
	Clubs(0.1);
	
	private final double score;
	
	private Suit(double score){
		this.score = score;
	}
	
	public double getSuitScore(){
		return this.score;
	}
}
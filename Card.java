public class Card{
	private Suit suit;
	private  Rank value;
	
	public Card(Suit suit, Rank value){
		this.suit = suit;
		this.value = value;
	}
	
	public Suit getSuit(){
		return this.suit;
	}
	public Rank getValue(){
		return this.value;
	}
	public String toString(){
		return this.value + " of " + this.suit;
	}
	public double calculateScore(){
		double score = this.getSuit().getSuitScore() + this.getValue().getRankScore();
		return score;
	}
}
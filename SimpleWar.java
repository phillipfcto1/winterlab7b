public class SimpleWar{
	public static void main(String[] args){
		//create new deck
		Deck deck = new Deck();
		deck.shuffle();
		
		//initialize player points
		int player1Points = 0;
		int player2Points = 0;

		
		while(deck.length() > 0){
			
			//draw a card and update the score for player1
			Card topCard1 = deck.drawTopCard();
			double score1 = topCard1.calculateScore();
			
			//draw a card and update the score for player1
			Card topCard2 = deck.drawTopCard();
			double score2 = topCard2.calculateScore();
			
			//print card and score for player1
			System.out.println(topCard1);
			System.out.println(score1);
		
			//print card and score for player2
			System.out.println(topCard2);
			System.out.println(score2);
		
			//check which score is higher
			if(score1 > score2){
				System.out.println("\n" + "Player1 Wins the round!" + "(" + score1 + ")");
				player1Points++;
			} 
			else{
				System.out.println("\n" + "Player2 Wins the round!" + "(" + score2 + ")");
				player2Points++;
			}
			//print player points
			System.out.println(player1Points);
			System.out.println(player2Points + "\n");
		}
		//check who has th highest points and congratulate them
		if(player1Points > player2Points){
		System.out.println("Player1 Wins!!!");
		}
		else{
		System.out.println("Player2 Wins!!!");
		}
	}
}